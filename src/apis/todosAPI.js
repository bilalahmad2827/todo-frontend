import { request } from "./request";

const PATH = "/graphql";

export const getTodos = () => {
  const query = {
    query: `
      query {
        todos {
          id, body, completed
        }
      }
    `,
  };

  return request.post(PATH, query);
};

export const createTodo = (data) => {
  const query = {
    query: `
    mutation {
      createTodo(input: {
        body: "${data.body}"
      }){
        todo {
          id,
          body,
          completed,
        }
        errors
      }
    }
    `,
  };

  return request.post(PATH, query);
};

export const updateTodo = (data) => {
  const query = {
    query: `
    mutation {
      updateTodo(input: {
        id: ${data.id},
        body: "${data.body}",
        status: ${data.status}
      }){
        todo {
          id,
          body,
          completed,
        }
        errors
      }
    }
    `,
  };

  return request.post(PATH, query);
};

export const deleteTodo = (id) => {
  const query = {
    query: `
    mutation {
      deleteTodo(input: {
        id: ${id}
      }){
        errors
      }
    }
    `,
  };

  return request.post(PATH, query);
};
